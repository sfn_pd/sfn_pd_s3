# SparkleFormation Pack: sfn_pd_s3

This SparkleFormation Pack helps you using basic things like Parameters, Outputs, Tags within your SparkleFormation Packs and Templates

## Getting Started

You can find simple working examples in the [generic](/generic) directory of this project. These examples are also used to test each commit to this repository.

### Prerequisites

To use this pack you have to add sfn_pd_s3 to your

1. Gemfile (or .gemspec if used within another gem) - to make sure this gem is installed
2. .sfn file or stack_master.yml - to make sure this pack is used

See also:
* [SparkleFormation Documentation](http://www.sparkleformation.io/docs/sparkle_formation/sparkle-packs.html) for SparklePacks
* [StackMaster Documentation](http://www.sparkleformation.io/docs/sfn/sparkle-packs.html#enabling-sparklepacks) for SparklePacks

## Built With

* [Sparkleformation](http://www.sparkleformation.io) - The DSL this Pack is written in

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. 

Given a version number MAJOR.MINOR.PATCH, increment the:

* MAJOR version when you make incompatible API changes,
* MINOR version when you add functionality in a backwards-compatible manner, and
* PATCH version when you make backwards-compatible bug fixes.

Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.

Make sure to set sfn_pd_s3/version.rb to your target Version.

If you start developing for a new Version, start a new branch target/MAJOR.MINOR.PATCH based on master if not exist - make sure to edit the version. Than start a new feature branch based on the target branch. Develop your feature inside your feature branch and merge it when ready into the target branch.

For the versions available, see the tags on this repository

## Authors

* **Patrick Domnick** - *Initial work* - [Gitlab](https://gitlab.com/Crapatrick)


## License

See the [LICENSE.md](LICENSE) file for details

## Acknowledgments

* To be continued ;-)