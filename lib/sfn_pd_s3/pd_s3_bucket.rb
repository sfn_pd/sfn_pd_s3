class SparkleFormation
    module SparkleAttribute
        module Aws
    
            # A small helper method for adding the specific named
            # parameter struct with the custom type
            def _pd_s3_bucket(_name, _config = {})
                
                #############################
                # Initialize:
                    
                    _config[:definition] ||= {}
                                        
                    _definition = _config[:definition]
                    _definition[:Properties] ||= {}
                    _properties = _definition[:Properties]

                # End Initialize
                #############################

                if _config[:kmskey]
                    _properties[:BucketEncryption] = {
                        :ServerSideEncryptionConfiguration => [
                            :ServerSideEncryptionByDefault => {
                                :KMSMasterKeyID => _config[:kmskey],
                                :SSEAlgorithm => "aws:kms"
                            }
                        ]
                    }
                end
                
                if _config[:aes256].to_s == "true"
                    _properties[:BucketEncryption] = {
                        :ServerSideEncryptionConfiguration => [
                            :ServerSideEncryptionByDefault => {
                                :SSEAlgorithm => "AES256"
                            }
                        ]
                    }
                end

                raise!("[Error] _properties[:BucketEncryption] or _config[:kmskey] must be set !!!") unless _properties[:BucketEncryption]

                #############################
                # S3Bucket:
                
                    _properties[:BucketName] = "TestBucket"
                    _definition[:Properties] = _properties
                
                # End S3Bucket
                #############################

            end
            alias_method :pd_s3_bucket!, :_pd_s3_bucket
        end
    end
end