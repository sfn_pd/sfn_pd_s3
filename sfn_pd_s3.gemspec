$LOAD_PATH.unshift File.expand_path(File.dirname(__FILE__)) + '/lib/'
require 'sfn_pd_s3/version'
Gem::Specification.new do |s|
  s.name = 'sfn_pd_s3'
  if ENV['CI_COMMIT_TAG']
    s.version = ENV['CI_COMMIT_TAG'].sub("v", "")
  elsif ENV['CI_COMMIT_REF_NAME'].include?('target/')
    git_version = ENV['CI_COMMIT_REF_NAME'].sub("target/", "")
    if  git_version == Version::VERSION
      s.version = "#{ENV['CI_COMMIT_REF_NAME']}.pre".sub("target/", "")
    else
      puts("[Error] Please make sure to set version to new target version")
      exit 1
    end
  elsif ENV['CI_COMMIT_REF_NAME'].include?('master')
    s.version = "#{Version::VERSION}.#{ENV['CI_COMMIT_REF_NAME']}"
  elsif ENV['CI_COMMIT_REF_NAME'].include?('feature/')
    s.version = "#{Version::VERSION}.feature.#{ENV['CI_COMMIT_REF_NAME'].sub('feature/','')}"
  else
    s.version = "#{Version::VERSION}.pre"
  end
  s.summary = 'Sparklefromation Simple Storage Service Pack'
  s.author = 'Patrick Domnick'
  s.email = 'patrickfdomnick@gmail.com'
  s.homepage = 'https://gitlab.com/sfn_pd/sfn_pd_s3'
  s.description = 'Sparklefromation Simple Storage Service Pack'
  s.license = 'MIT'
  s.add_dependency 'sfn', '>= 3.0', '< 4.0'
  s.add_dependency 'json', '>= 2.0', '< 3.0'
  s.files = Dir['{lib,docs}/**/*'] + %w(sfn_pd_s3.gemspec README.md CHANGELOG.md LICENSE)
end